const postCredentials = (url) => {
  const username = document.getElementById('username').value;
  const password = document.getElementById('password').value;

  const errorMessage = document.getElementById('errorMessage');

  postRequest(url, {
    username: username,
    password: password
  }).then(response => {
    if(response.error) {
      errorMessage.textContent = response.error;
    } else if (response.redirect) {
      window.location.href = response.redirect;
    }
  });
};

